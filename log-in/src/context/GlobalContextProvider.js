import React from 'react';
import { AuthContextProvider } from './AuthContext';
import { AxiosContextProvider } from './AxiosContext';
import { LoginContextProvider } from './LoginContext';
import { UserContextProvider } from './UserContext';

export const GlobalContextProvider = (props) => {
    return(
        <AuthContextProvider>
            <AxiosContextProvider>
                <UserContextProvider>
                    <LoginContextProvider>
                        {props.children}
                    </LoginContextProvider>
                </UserContextProvider>
            </AxiosContextProvider>
        </AuthContextProvider>
    )
}






