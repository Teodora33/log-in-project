import React, { createContext, useContext } from 'react';
import { AxiosContext } from './AxiosContext';
import { AuthContext } from './AuthContext';
import { UserContext } from './UserContext';

const LoginContext = createContext();

const LoginContextProvider = (props) => {
    const { axiosInstance } = useContext(AxiosContext)
    const { saveToken } = useContext(AuthContext)
    const { saveCurrentUserId } = useContext(UserContext)

    const login = (data) => {
        return axiosInstance({
            method: 'POST',
            url: 'auth',
            data: data
        })
        .then(response => {
            saveCurrentUserId(response.data.user._id)
            saveToken(response.data.token)
        });
    }

    const providerValue = {
        login: login
    }

    return(
        <LoginContext.Provider value={providerValue}>
            {props.children}
        </LoginContext.Provider>
    )
}

const LoginConsumer = LoginContext.Consumer;

export { LoginContext, LoginContextProvider, LoginConsumer}