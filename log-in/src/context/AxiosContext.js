import React, { createContext, useContext } from 'react';
import { AuthContext } from './AuthContext';
import axios from 'axios';

const AxiosContext = createContext();

const AxiosContextProvider = (props) => {

    const { token } = useContext(AuthContext);

    const instance = axios.create({
        baseURL: 'http://localhost:5000/api/',
        headers:{
            authorization: 'Bearer '.concat(token)
        }
    })

    const providerValue = {
        axiosInstance: instance
    }

    return(
        <AxiosContext.Provider value={providerValue}>
            {props.children}
        </AxiosContext.Provider>
    )
}

const AxiosConsumer = AxiosContext.Consumer;

export { AxiosContext, AxiosContextProvider, AxiosConsumer};