import React, { createContext, useContext, useState, useEffect, useCallback } from 'react';
import { AxiosContext } from './AxiosContext';
import { AuthContext } from './AuthContext';

const UserContext = createContext();

const UserContextProvider = (props) => {
    const { axiosInstance } = useContext(AxiosContext);
    const { token } = useContext(AuthContext);

    const [ currentUserId, setCurrentUserId ] = useState('');
    const [ employees, setEmployees ] = useState('');
    const [ isAdmin, setIsAdmin ] = useState(false);

    const saveCurrentUserId = (userId) => {
        setCurrentUserId(userId)
    }

    const getUserFromToken = () => {

        return axiosInstance({
            method: 'GET',
            url: 'auth/currentUser'
        }) 
        .then(response => {
            setCurrentUserId(response.data._id)
            setIsAdmin(response.data.isAdmin)
        });
    }

    useEffect(() => {
        if (token) {
            if(!isAdmin || currentUserId === '') {
                getUserFromToken()
            }
        }
    })

    const getUsers = useCallback(() => {
        return axiosInstance({
            method: 'GET',
            url: 'users'
        }).then(response => {
            return setEmployees(response.data)
        })
    }, [axiosInstance])

    const getUser = (id) => {
        return axiosInstance({
            method: 'GET',
            url: `users/${id}`
        }).then(response => {
            return response
        })
    }

    const postUser = (data) => {
        return axiosInstance({
            method: 'POST',
            url: `users`,
            data: data
        }).then(response => {
            if(response.data) getUsers()
        })
    }
        
    const patchUser = (id, data) => {
        return axiosInstance({
            method: 'PATCH',
            url: `users/${id}`,
            data: data,
        }).then(response => {
            return getUsers()
        })
    }

    const deleteUser = (id) => {
        return axiosInstance({
            method: 'DELETE',
            url: `users/${id}`
        }).then(response => {
            if(response.data) getUsers() 
        })
    }

    const providerValue = {
        currentUserId: currentUserId,
        isAdmin: isAdmin,
        employees: employees,
        setIsAdmin: setIsAdmin,
        saveCurrentUserId: saveCurrentUserId,
        getUserFromToken: getUserFromToken,
        getUsers: getUsers,
        getUser: getUser,
        postUser: postUser,
        patchUser: patchUser,
        deleteUser: deleteUser
    }

    return(
        <UserContext.Provider value={providerValue}>
            {props.children}
        </UserContext.Provider>
    )
}

const UserConsumer = UserContext.Consumer;

export { UserContext, UserContextProvider, UserConsumer}
