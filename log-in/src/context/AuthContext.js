import React, {createContext, useState }  from 'react';

const AuthContext = createContext();

const AuthContextProvider = (props) => {
    
    const [token, setToken] = useState(localStorage.getItem('token'));

    const saveToken = token => {
        setToken(token);
        localStorage.setItem('token', token);
    }

    const getUser = token => {
        setToken(token);
        localStorage.getItem('token', token)
    }

    const removeToken = () => {
        setToken('');
        localStorage.removeItem('token')
    }

    const providerValue = {
        token: token,
        saveToken: saveToken,
        getUser: getUser,
        removeToken: removeToken
    }

    return(
        <AuthContext.Provider value={providerValue}>
            {props.children}
        </AuthContext.Provider>
    )
}

const AuthConsumer = AuthContext.Consumer;
export { AuthContext, AuthContextProvider, AuthConsumer};

