import React, { useState, useContext, useEffect } from 'react';
import { UserContext } from '../../../context/UserContext';
import Input from '../../atoms/input/input';
import Textarea from '../../atoms/textarea/textarea';
import Buton from '../../atoms/button/button';
import Modal from '../../organisms/modal/Modal';
import useModal from '../../organisms/modal/useModal';
 
function ProfileForm ({hide, userId, admin}) {
    const {isShowingUpdate, toggleUpdate} = useModal();
    const {isShowingDelete, toggleDelete} = useModal();
    const {isShowingCreate, toggleCreate} = useModal();
    const { getUser, postUser, patchUser, deleteUser } = useContext(UserContext);
    const [photo, setPhoto] = useState({ preview: "", raw: ""});
    const [createDisabled, setCreateDisabled] = useState(true)
    const [createUser, setCreatedUser] = useState({
        photo: '',
        firstName: '',
        lastName: '',
        email: '',
        password: '',
        phoneNumber: '',
        adress: '',
        jobTitle: '',
        about: ''
    }); 

    const [updateUser, setUpdatedUser] = useState({
        photo: '',
        firstName: '',
        lastName: '',
        email: '',
        phoneNumber: '',
        adress: '',
        jobTitle: '',
        about: ''
    });  
    
    const setUser = userId ? setUpdatedUser : setCreatedUser;
    function formHandler(fieldName, value){
        setUser({
            ...displayUser,
            [fieldName]: value
        })
        isButtonDisabled()
    }

    const isButtonDisabled = () => {
        if(createUser.firstName === '' 
            || createUser.lastName === ''
            || createUser.email === ''
            || createUser.phoneNumber === ''
            || createUser.adress === ''
            || createUser.jobTitle === ''
            || createUser.about === '') setCreateDisabled(true)
        else setCreateDisabled(false)
    }

    useEffect(() => {
        if(userId) {
            getUser(userId).then(response => {
                setUpdatedUser(response.data)
            })
        }
    },[getUser, setUpdatedUser, userId]);

    function handleEdit(){
        const mappedData = {
            photo: photo.raw,
            firstName: updateUser.firstName,
            lastName: updateUser.lastName,
            email: updateUser.email,
            phoneNumber: updateUser.phoneNumber,
            adress: updateUser.adress,
            jobTitle: updateUser.jobTitle,
            about: updateUser.about
        }
        const formData = new FormData();
        if(mappedData.photo){
            formData.append('photo', photo.raw);
        }
        
        Object.keys(mappedData).forEach(key => {
            if(key !== 'photo' && mappedData[key]){
                if(typeof mappedData[key] !== 'object'){
                    formData.append(key, mappedData[key])
                } else formData.append(key, JSON.stringify(mappedData[key]))
            }
        })
        patchUser(userId,formData)
    }

    function handleDelete(){
        deleteUser(userId)
    }

    function handleCreate(){
        const mappedData = {
            photo: photo.raw,
            firstName: createUser.firstName,
            lastName: createUser.lastName,
            email: createUser.email,
            password: createUser.password,
            phoneNumber: createUser.phoneNumber,
            adress: createUser.adress,
            jobTitle: createUser.jobTitle,
            about: createUser.about
        }
        const formData = new FormData();
        formData.append('photo', photo.raw);
        
        Object.keys(mappedData).forEach(key => {
            if(key !== 'photo' && mappedData[key]){
                if(typeof mappedData[key] !== 'object'){
                    formData.append(key, mappedData[key])
                } else formData.append(key, JSON.stringify(mappedData[key]))
            }
        })
        postUser(formData)
    }

    const fileSelectedHandler = (e) => {

        if (e.target.files.length) {
            setPhoto({
                preview: URL.createObjectURL(e.target.files[0]),
                raw: e.target.files[0]
            });
        }
    };

    const displayUser = userId ? updateUser : createUser;
    return (
        <div className='profile'>
            <div >
                <label htmlFor='upload-button'>
                    {photo.preview ? <img className='profile__photo' src={photo.preview} style={{width:'90px'}} alt='Profile'/> : (
                        <>
                        <span className='fa-stack fa-2x mt-3 mb-2'>
                            <i className='fas fa-circle fa-stack-2x' />
                            <i className='fas fa-store fa-stack-1x fa-inverse' />
                        </span>
                        <p className='profile__text--upload'>Upload photo</p>
                    </>)}
                </label>
                <input 
                    type='file' 
                    id='upload-button' 
                    name='photo'
                    style={{ display: 'none' }} 
                    onChange={fileSelectedHandler} 
                    />
            </div>
            <div className='float'>
                <div className='float__wrapper'>
                        <Input 
                            type='text'
                            placeholder='First name'
                            value={displayUser.firstName}
                            inputHandler={(value) => formHandler('firstName', value)}
                        />
                </div>
                <div className='float__wrapper'>
                        <Input
                            type='text'
                            placeholder='Last name'
                            value={displayUser.lastName}
                            inputHandler={(value) => formHandler('lastName', value)}
                        />
                </div>
                <div className='float__wrapper'>
                        <Input
                            type='email'
                            placeholder='Email'
                            value={displayUser.email}
                            inputHandler={(value) => formHandler('email', value)}
                        />
                </div>
                {!userId &&
                <div className='float__wrapper'>
                        <Input
                            type='password'
                            placeholder='Password'
                            value={displayUser.password}
                            inputHandler={(value) => formHandler('password', value)}
                        />
                </div>}
                <div className='float__wrapper'>
                        <Input
                            type='text'
                            placeholder='Phone number'
                            value={displayUser.phoneNumber}
                            inputHandler={(value) => formHandler('phoneNumber', value)}
                        />  
                </div>      
                <div className='float__wrapper'>
                        <Input 
                            type='text'
                            placeholder='Adress'
                            value={displayUser.adress}
                            inputHandler={(value) => formHandler('adress', value)}
                        />
                </div>
                    <div className='float__wrapper'>
                        <Input 
                            type='text'
                            placeholder='Job title'
                            value={displayUser.jobTitle}
                            inputHandler={(value) => formHandler('jobTitle', value)}
                        />
                    </div>
                    <div className='float__wrapper'>
                        <Textarea
                            type='textarea'
                            placeholder='Description'
                            value={displayUser.about}
                            inputHandler={(value) => formHandler('about', value)}

                        />
                    </div>
            </div>
            <div className='profile__btn'>
                {!userId ? (
                    <div className='profile__btn--create'>
                        <div className='content'>
                            <Buton 
                                disabled = {createDisabled}
                                className={'button--green'}
                                onClick={() => {
                                    toggleCreate();
                                    handleCreate();
                                }}
                                label='Create'
                            />
                        </div>
                    </div>
                ) : (
                    <div className='profile__btn--wrapper'>
                        <div className='profile__btn--update'>
                            <div className='content'>
                                    <Buton 
                                        className={'button'}
                                        onClick={() => {
                                            toggleUpdate();
                                            handleEdit();
                                        }}
                                        label='Update'
                                    />
                            </div>
                        </div>
                            {admin &&
                                <div className='profile__btn--delete'>
                                    <div className='content'>
                                        <Buton 
                                            className={'button--red'}
                                            onClick={() => {
                                                toggleDelete();
                                                handleDelete();
                                            }} 
                                            label='Delete'
                                        />
                                    </div>
                                </div>
                            }
                    </div>
                )}   
            </div>
        



            {/* <div className='profile__btn'>
                {!userId ? (
                    <div className='profile__btn--create'>
                        <div className='content'>
                    <Buton 
                        disabled = {createDisabled}
                        className={'button--green'}
                        onClick={() => {
                            toggleCreate();
                            handleCreate();
                        }}
                        label='Create'
                    />
                        </div>
                    </div>
                ) : (
                    <div className='profile__btn--wrapper'>
                        <div style={{width: '40%'}}>
                            <Buton 
                                onClick={() => {
                                    toggleUpdate();
                                    handleEdit();
                                }}
                                label='Update'
                            />
                        </div>
                        {admin &&
                        <div style={{width: '40%'}}>
                            <Buton 
                                className={'button--red'}
                                onClick={() => {
                                    toggleDelete();
                                    handleDelete();
                                }} 
                                label='Delete'
                            />
                        </div>}
                    </div>
                )}    
            </div> */}
            <Modal
                isShowing={isShowingCreate}
                hide={toggleCreate && hide}
                type= 'createModal'
            />  
            <Modal
                isShowing={isShowingUpdate}
                hide={toggleUpdate && hide}
                type= 'updateModal'
            />  
            <Modal
                isShowing={isShowingDelete}
                hide={toggleDelete && hide}
                type= 'deleteModal'
            />
           </div>
    )
}

export default ProfileForm;