import React from 'react';
import Modal from '../../organisms/modal/Modal';
import useModal from '../../organisms/modal/useModal';
import msg from '../../../assets/msg.png';
import location from '../../../assets/location.png';
import edit from '../../../assets/edit.png';
function ProfileLayout (props) {
  const {employee, pathToImage, admin} = props;
  const {isShowing, toggle} = useModal();

    function handleOpenEdit(e) {
      e.preventDefault();
      toggle()
  }
  
    return (
      <div className='profile__wrapper'> 
        <div>
          {admin && <img className='profile__wrapper--edit' alt='Edit' src={edit} onClick={handleOpenEdit}/>}
        </div>
        <div className='profile__wrapper--basic'>
          <div>
            <img className='profile__wrapper--basic--img'src={pathToImage+employee.photo} alt='Profile'/>  
          </div>
          <div className='profile__wrapper--basic--name'> {employee.firstName} {employee.lastName}</div>
          <div className='profile__wrapper--basic--job'>{employee.jobTitle}</div>
        </div>
        <div className='profile__wrapper--line' ></div>
        <div className='profile__wrapper--info'>
          <div className='profile__wrapper--info--title'>
            <p>A little bit about {employee.firstName}</p>
          </div>
          <div className='profile__wrapper--info--about'>{employee.about}</div>
          <div className='profile__wrapper--info--email'>
            <img className='profile__wrapper--info--email--img' alt='Email icon' src={msg}/> 
            {employee.email}
          </div>
          <div className='profile__wrapper--info--location'>
            <img className='profile__wrapper--info--email--img' alt='Location icon' src={location}/> 
            {employee.adress}
          </div>
        </div>
        <Modal
          admin={admin}
          isShowing={isShowing}
          hide={toggle}
          type='profileModal'
          userId={employee._id} 
        />
      </div>
    )
}

export default ProfileLayout;