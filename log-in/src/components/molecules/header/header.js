import React, { useContext } from 'react';
import { AuthContext } from '../../../context/AuthContext';
import { UserContext } from '../../../context/UserContext';
import { useHistory } from 'react-router-dom';
import Modal from '../../organisms/modal/Modal';
import useModal from '../../organisms/modal/useModal';
import logo from '../../../assets/logo.png';
import profile from '../../../assets/profile.png';
import logout from '../../../assets/logout.png';
import create from '../../../assets/create.png';

function Header ({admin}) {
    const history = useHistory();
    const {isShowing, toggle} = useModal();
    const {isShowingCreate, toggleCreate} = useModal();
    const { removeToken } = useContext(AuthContext);
    const { currentUserId, setIsAdmin } = useContext(UserContext);
    
    function logoutUser() {
        history.push('/')
        setIsAdmin(false)
        removeToken()
    }

    function navigateToHome() {
        history.push('/')
    }
    
    return (
        <header className='header'>
            <div className='header__logo--wrapper'>
                <img className='header__logo--wrapper--img' alt='Logo' src={logo} onClick={navigateToHome}/>
            </div>
            {admin && <img className='header__logo--create' alt='Create logo' src={create} onClick={toggleCreate}/>}
            <img className='header__logo--profile' alt='Edit profile' src={profile} onClick={toggle}/>
            <img  className='header__logo--logout' alt='Logout'src={logout} onClick={logoutUser}/>  
            <Modal
                isShowing={isShowingCreate}
                hide={toggleCreate}
                type='createProfile'
            />
            <Modal
                isShowing={isShowing}
                hide={toggle}
                type='profileModal'
                userId={currentUserId}
                admin={admin}
            />
        </header>        
    )
}

export default Header;