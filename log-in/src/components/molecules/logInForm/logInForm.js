import React, { useState, useContext } from 'react';
import { LoginContext } from '../../../context/LoginContext';
import logo from '../../../assets/logo.png';
import userIcon from '../../../assets/user.png';
import passwordIcon from '../../../assets/padlock.png';
import Input from '../../atoms/input/input';
import Button from '../../atoms/button/button';

function LogInForm () {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const { login } = useContext(LoginContext);
    const [error, setError] = useState(false);
    const [createDisabled, setCreateDisabled] = useState(true);

    const isButtonDisabled = () => {
        if(email === '' || password === '') setCreateDisabled(true)
        else setCreateDisabled(false)
    }
    function handleEmailChange(email) {
        isButtonDisabled()
        setEmail(email)
    }

    function handlePasswordChange(password) {
        isButtonDisabled()
        setPassword(password)
    }

    const logInUser = (e) => {
        e.preventDefault();
        
        const userData = {
            email: email,
            password: password
        }
        
        login(userData).catch(err => {
            setError(true)
        })
    }
    
    return (
        <div className='login'>
            <div className='login--wrapper__heading'>
                <h1 className='login__heading'>Welcome</h1>
                <img className='logo'src={logo} alt='logo' />
            </div>
            <form>
                <div className='login__form--input'>
                    <img className='login__form--input--img' src={userIcon} alt='userIcon'/>
                    <Input 
                        type='email' 
                        placeholder='Type your email here' 
                        value={email}
                        inputHandler={handleEmailChange} 
                    />   
                </div>              
                <div className='login__form--input'>
                    <img className='login__form--input--img' src={passwordIcon} alt='passwordIcon'/>
                    <Input 
                        type='password' 
                        placeholder='Type your password here' 
                        value={password}
                        inputHandler={handlePasswordChange} 
                    />
                </div>
                <div className='error'>
                    {error && <p className='error--msg'> <span role="img" aria-label='warrning'> &#10060; </span>Invalid email or password</p>}  
                </div>
                <Button 
                    disabled = {createDisabled}
                    onClick={(e) => logInUser(e)}
                    label='SIGN IN'
                />
            </form>
        </div>
    )
}

export default LogInForm;


