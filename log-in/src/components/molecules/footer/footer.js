import React from 'react';

import logo from '../../../assets/logo.png';

function Footer () {

    return (
        <footer className='footer'>
            <div className='footer__logo--wrapper'>
                <img className='footer__logo--wrapper--img' alt='Logo' src={logo}/>
            </div>
            <div className='footer__wrapper--text'>
                <p>Copyright © 2020 All rights reserved. Teodora Cumpf</p>  
            </div>
        </footer>
        
    )
}

export default Footer;