import React from 'react';
import { useHistory } from 'react-router-dom';
import msg from '../../../assets/msg.png';

function EmployeeCard ({employee}) {
    const history = useHistory();
    const pathToImage = `http://localhost:5000/uploads/`;

    function navigateToProfile() {
        history.push("/profile", {
            pathToImage,
            employee,
        })
    }

    return (
        <div className='employee' onClick={navigateToProfile}>
            <div className='employee__wrapper'>
                <div className='employee__wrapper--img'>
                    <img className='employee__wrapper--img--profile' src={pathToImage+employee.photo} alt='Profile'/>
                </div>
                <div className='employee__wrapper--text'>
                    <p>{employee.firstName} {employee.lastName}</p>
                    <p className='employee__wrapper--text--title'>{employee.jobTitle}</p>
                </div>
                    <p className='employee__wrapper--about'>{employee.about}</p>
                <div className='employee__wrapper--email'>
                    <img className='employee__wrapper--email--img' alt='Email icon' src={msg}/> 
                    {employee.email}
                </div>
            </div>
        </div>    
    )
}
export default EmployeeCard;