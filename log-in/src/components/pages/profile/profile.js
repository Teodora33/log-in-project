import React, { useContext } from 'react';
import { UserContext } from '../../../context/UserContext';
import Header from '../../molecules/header/header';
import Footer from '../../molecules/footer/footer';
import ProfileLayout from '../../molecules/profileLayout/profileLayout';
 
function Profile ({employee, pathToImage}) {
    const { isAdmin } = useContext(UserContext)
    
    return (
        <div className='wrapper'>
            <Header admin={isAdmin}/>
            <div className='wrapper--profile'>
                <ProfileLayout employee={employee} pathToImage={pathToImage} admin={isAdmin}/>
            </div>
            <div className='wrapper__footer'>
                <Footer/>
            </div>
        </div>
    )
}

export default Profile;