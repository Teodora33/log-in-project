import React from 'react';
 
import LogInForm from '../../molecules/logInForm/logInForm';

function LogIn() {

  return (
    
    <div className='login--page'>
        <LogInForm/>
    </div>
  ) 
}
 
export default LogIn;