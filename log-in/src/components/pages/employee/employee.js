import React, { useContext, useEffect } from 'react';
import { UserContext } from '../../../context/UserContext';
import Header from '../../molecules/header/header';
import EmployeeCard from '../../molecules/employeeCard/employeeCard';
import Footer from '../../molecules/footer/footer';

function EmployeeHome () {

    const { getUsers, isAdmin, employees } = useContext(UserContext)

    useEffect(() => {
        getUsers()
    },[getUsers]);  

    const displayEmployees = () => {
        if(employees.length > 0){
            return employees.map( employee => {
                return <EmployeeCard key={employee._id} employee={employee} admin={isAdmin}/>
            })
        }
    }
    
    return (    
        <div className='wrapper'>
            <Header admin={isAdmin}/>
            <div className='wrapper--cards'>
                {displayEmployees()}
            </div>
            <div className='wrapper__footer'>
                <Footer/>
            </div>
        </div>  
    )
}

export default EmployeeHome;
