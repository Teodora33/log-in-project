import React from 'react';

function Button (props) {

  const className = props.disabled ? 'button__disabled' : props.className ? props.className : 'button';
  return (
    <div>
      <button 
        disabled={props.disabled}
        className={className} 
        type='submit' 
        onClick={props.onClick}
      > 
      {props.label}
      </button>
    </div>
  )
}

export default Button;

