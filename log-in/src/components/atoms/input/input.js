import React, { useEffect, useState } from 'react';

function Input ({type, placeholder, inputHandler, value}) {

    const [active, setActive] = useState(false);

    const handleFocus = () => {
        setActive(true);
      };
   
    useEffect(() => {
        if(value > 0 || value > '') setActive(true)
    }, [value])
    
    return(
        <div>
            <label className={active ? 'float__wrapper--label_floating' : 'float__wrapper--label'}>{placeholder}</label>
            <input
                type={type}
                value={value}
                onChange={e => inputHandler(e.target.value)}
                onFocus={() => handleFocus()}
            />
        </div>   
    )
};

export default Input;