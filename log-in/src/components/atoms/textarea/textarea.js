import React from 'react';

function Textarea ({type, placeholder, inputHandler, value}) {
   
    return(
        <div>           
            <textarea 
                className='textarea'
                placeholder={placeholder}
                type={type}
                value={value}
                onChange={e => inputHandler(e.target.value)}
            />
        </div>   
    )
};

export default Textarea;