import React from 'react';
import ProfileForm from '../../molecules/profileForm/profileForm';
import accepted from '../../../assets/accepted.png';
import deleted from '../../../assets/delete.png';

const Modal = ({isShowing, hide, type, userId, admin}) => {
  const profileModal = () => {
    return(
      <React.Fragment>
          <div className='modal__overlay'/>
          <div className='modal__wrapper'>
            <div className='modal'>
              <div className='modal__wrapper__content'>
                <button 
                  type='button' 
                  className='modal__wrapper__content--button' 
                  onClick={hide}>
                  <span aria-hidden='true'>&times;</span>
                </button>  
                <ProfileForm hide={hide} userId={userId} admin={admin}/>
              </div>
            </div>
          </div>
        </React.Fragment>
    )
  }

  const createProfile = () => {
    return(
      <React.Fragment> 
          <div className='modal__overlay'/>
          <div className='modal__wrapper'>
            <div className='modal'>
              <div className='modal__wrapper__content'>
                <button 
                  type='button' 
                  className='modal__wrapper__content--button' 
                  onClick={hide}>
                  <span aria-hidden='true'>&times;</span>
                </button>
                <ProfileForm hide={hide}/>
              </div>
            </div>
          </div>
        </React.Fragment>
    )
  }

  const createModal = () => {
    return(
      <React.Fragment>
          <div className='modal__overlay--accepted' />
          <div className='modal__wrapper' onClick={hide}>
            <div className='modal__wrapper--updated'>
              <div className='modal__wrapper__content'>
                <img
                  className='modal__wrapper__content--img' 
                  src={accepted} 
                  alt='accepted' 
                />
                <p>New profile has been created!</p>
              </div>
            </div>
          </div>
        </React.Fragment>
    )
  }

  const updateModal = () => {
    return(
      <React.Fragment>
          <div className='modal__overlay--accepted'/>
          <div className='modal__wrapper' onClick={hide}>
            <div className='modal__wrapper--updated'>
              <div className='modal__wrapper__content'>
                <img
                  className='modal__wrapper__content--img' 
                  src={accepted} 
                  alt='accepted' 
                />
                <p>Your profile has been updated!</p>
              </div>
            </div>
          </div>
        </React.Fragment>
    )
  }

  const deleteModal = () => {
    return(
      <React.Fragment>
          <div className='modal__overlay--accepted'/>
          <div className='modal__wrapper' onClick={hide}>
            <div className='modal__wrapper--updated'>
              <div className='modal__wrapper__content'>
                <img
                  className='modal__wrapper__content--img' 
                  src={deleted} 
                  alt='deleted' 
                />
                <p>Profile has been successfully deleted!</p>
              </div>
            </div>
          </div>
        </React.Fragment>
    )
  }

  const renderModal = () => {
    switch(type){
      case 'profileModal':
        return profileModal();
      case 'updateModal':
        return updateModal();
      case 'deleteModal':
        return deleteModal();
      case 'createProfile' :
        return createProfile();
      case 'createModal' :
        return createModal()
      default:
        return;
    }
  }

  return(
    <React.Fragment>
      {(isShowing && 
        renderModal())
      }
    </React.Fragment>
  )
}

export default Modal;