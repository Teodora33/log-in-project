import { useState } from 'react';
import { useHistory } from 'react-router-dom';

const useModal = () => {
  const history = useHistory();

  const [isShowing, setIsShowing] = useState(false);
  const [isShowingUpdate, setIsShowingUpdate] = useState(false);
  const [isShowingDelete, setIsShowingDelete] = useState(false);
  const [isShowingCreate, setIsShowingCreate] = useState(false);

  function toggle() {
    setIsShowing(!isShowing);
  }

  function toggleUpdate() {
    setIsShowingUpdate(!isShowingUpdate);
  }

  function toggleDelete() {
    history.push('/')
    setIsShowingDelete(!isShowingDelete);
  }

  function toggleCreate() {
    setIsShowingCreate(!isShowingCreate);
  }
  return {
    isShowing,
    isShowingUpdate,
    isShowingDelete,
    isShowingCreate,
    toggle,
    toggleUpdate,
    toggleDelete,
    toggleCreate,
  }
};

export default useModal;