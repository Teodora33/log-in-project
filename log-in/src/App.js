import React, { useContext } from 'react';
import { Router, Route, Switch } from 'react-router-dom';
import history from './RouterHistory';
import { AuthContext } from './context/AuthContext';
import './scss/main.scss';

import LogIn from './components/pages/login/logIn';
import EmployeeHome from './components/pages/employee/employee';
import Profile from './components/pages/profile/profile';

function App() {
  const { token } = useContext(AuthContext)
  return (
      <Router history={history}>
        {token ? 
          <Switch>
            <Route exact path='/' component={EmployeeHome} />
            <Route path='/profile' render={props => <Profile {...props.location.state}/>}/>
          </Switch>
          :
          <Switch>
            <Route exact path='/' component={LogIn} />
          </Switch>
        }
        </Router>


  );
}

export default App;

