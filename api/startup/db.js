const mongoose = require('mongoose');

module.exports = function () {
    mongoose.connect('mongodb://localhost/loginapp')
    .then(() => console.log('Connected to MongoDB...'))
    .catch(() => console.log('Could not connect to MongoDB...'));
}