const express = require('express');
const router = express.Router();
const authController = require('../controllers/authController');
const tokenFromUser = require('../middleware/tokenFromUser');

router.post('/', authController.logIn);
router.get('/currentUser', tokenFromUser);

module.exports = router;
