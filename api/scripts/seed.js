const {User} = require('../models/user');
const userData = require('./seedObjects/usersData');
const mongoose = require('mongoose');

function seed() {
    return User.remove({}).then(() => {
        userData.forEach(user => User.create(user))
    })
}

mongoose.connect('mongodb://localhost/loginapp')
    .then(()=> console.log('Connected to MongoDB...'))
    .then(()=> seed())
    .then(()=> console.log('Seeding finished'))