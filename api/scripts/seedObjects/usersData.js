const mongoose = require('mongoose');

module.exports = [
    { 
        "_id": new mongoose.mongo.ObjectId("5f61cb72f9775b27c8ffdc07"),
        "photo":"person6.jpg",
        "firstName":"Teodora",
        "lastName": "Cumpf",
        "email": "teodora.cumpf@gmail.com",
        "password": "$2b$10$h4pWaaRxzzfK5Rd9qsaihOJRqy5G0jyLC.9N7UQDpK5fn1EtRQ/GK",
        "about": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
        "isAdmin": true,
        "phoneNumber": "0643267846",
        "adress": "Mileve Maric 23",
        "jobTitle": "Administrator",
        "__v":0
    },
    { 
        "_id": new mongoose.mongo.ObjectId("5fa26b762ba4e34d64076b91"),
        "photo":"person3.jpg",
        "firstName":"John",
        "lastName": "Don",
        "email": "john@gmail.com",
        "password": "$2b$10$h4pWaaRxzzfK5Rd9qsaihOJRqy5G0jyLC.9N7UQDpK5fn1EtRQ/GK",
        "about": "Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus.",
        "phoneNumber": "0643453846",
        "adress": "Danila Kisa 19",
        "jobTitle": "Web Developer",
        "__v":0
    },
    { 
        "_id": new mongoose.mongo.ObjectId("5f9bd1c8fe6d52480c3b60f5"),
        "photo":"jane.doe.png",
        "firstName":"Jane",
        "lastName": "Doe",
        "email": "jane@gmail.com",
        "password": "$2b$10$h4pWaaRxzzfK5Rd9qsaihOJRqy5G0jyLC.9N7UQDpK5fn1EtRQ/GK",
        "about": "Blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est.",
        "phoneNumber": "0636743846",
        "adress": "Zmaj Jovina 24",
        "jobTitle": "Graphic Designer",
        "__v":0
    },
    { 
        "_id": new mongoose.mongo.ObjectId("5fa2c5212ba4e34d64076b93"),
        "photo":"person2.jpg",
        "firstName":"Don",
        "lastName": "Huan",
        "email": "don@gmail.com",
        "password": "$2b$10$h4pWaaRxzzfK5Rd9qsaihOJRqy5G0jyLC.9N7UQDpK5fn1EtRQ/GK",
        "about": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
        "phoneNumber": "0693267846",
        "adress": "Seljackih Buna 420",
        "jobTitle": "QA Tester",
        "__v":0
    },
    { 
        "_id": new mongoose.mongo.ObjectId("5fa2c5cb2ba4e34d64076b94"),
        "photo":"person4.jpg",
        "firstName":"Angelina",
        "lastName": "Moor",
        "email": "angelina@gmail.com",
        "password": "$2b$10$h4pWaaRxzzfK5Rd9qsaihOJRqy5G0jyLC.9N7UQDpK5fn1EtRQ/GK",
        "about": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
        "phoneNumber": "0653267846",
        "adress": "Bate Brkica 69",
        "jobTitle": "Unity Developer",
        "__v":0
    },
    { 
        "_id": new mongoose.mongo.ObjectId("5fa2c6232ba4e34d64076b95"),
        "photo":"john.doe.png",
        "firstName":"Aleksandar",
        "lastName": "Tisma",
        "email": "tismans95@gmail.com",
        "password": "123456789",
        "about": "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.",
        "phoneNumber": "0693267846",
        "adress": "Mileve Maric 27",
        "jobTitle": "Mentor",
        "__v":0
    }
]